= Inverse Schedulers — Noise from Latents for Invoke AI

Many schedulers are invertible.
Run them backwards from a clean image, and you should get something that's not random, but it's like noise,
and if you supply it as noise to the denoising process you should be able to get your original image out again!

That's the theory, anyway.
It's a technique that's been used in papers like https://pix2pixzero.github.io/[pix2pix-zero].
diffusers does have an implementation of pix2pix-zero, but the whole kit requires a captioning model and cross-attention processors;
I wanted to see if I could get a node that just does the inverted denoise to the latents.

This is experimental exploratory code at this point:
use at your own risk, do not expect production quality, do not operate heavy machinery, etc etc.

See link:examples/Image%20to%20Noise%20to%20Image.json[Image to Noise to Image.json] for an example workflow.


== Requirements

https://invoke-ai.github.io/InvokeAI/[InvokeAI] version 3.5.
